# Property.ca Backend Coding Test
---------------------------------

As a potential hire for Property.ca, we want to get a gauge of your "real world" coding skills. We don't believe whiteboard algorithmic puzzles give an accurate portrayal of your ability to write good code. So here's a test that aligns more with the type of task you might encounter while working at Property.ca.


## Overview

You are tasked with building a simple application that exposes a few REST API endpoints that return JSON data. You are free to use either Javascript (Node.js), PHP, or Python. We have limited it to the languages we are most familiar with in order to ensure we can better review your solution. On top of that, use any framework, library, or database you wish.


## Data Sets

Three CSV files are provided in the `data` folder for use with this test:

- `addresses.csv`
- `areas.csv`
- `sales.csv`

It is your choice on whether you want to load this data into a database or just into the application's memory at run time.


## Required Endpoints

- `GET /sales/change-by-address` : Returns a list of addresses with the percentage change in sale price over the full time span included in the data set. The returned data should include: `name`, `percentageChange`, `timespan`.
- `GET /sales/change-by-area` : Return a list of areas with the average percentage change for all addresses in the area. The returned data should include: `name`, `percentageChange`, `timespan`.

All endpoints should accept a `sort` query parameter that can either sort ascending from greatest decrease to greatest increase (`sort=percentageChange`) or descending (`sort=-percentageChange`). The endpoint responses should be in JSON. If an area/address has more than two sales, use the oldest and the newest for the percentage change calculation. `timespan` should be an integer value of the number of days between the two sale dates.


## Process

1. Fork this repository.
2. Code your solution.
3. Commit your code to your fork (don't feel you need to complete this in one sitting, we won't evaluate your solution until you've told us it's complete).
4. Edit this README file to include instructions for running your code and any other notes you wish to share (design decisions, limitations, etc).
5. Send an email to your contact at Property.ca letting them know you are ready for your evaluation.


## Evaluation

If we are not able to get a version of your code up and running, we will not proceed with the evaluation. If you use any libraries make sure to include a `package.json`/`requirements.txt`/`composer.json`. Other databases aside from SQLite, we require a Docker file with the environment setup. Alternatively, setup your own hosted version of your solution, include the URL, and we can use that for the evaluation instead of running it locally on our machines.


## Questions?

Please either send an email to your Property.ca contact or open an issue on this repo and we will respond as soon as possible.
